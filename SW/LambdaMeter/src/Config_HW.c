/*
 * Config_HW.c
 *
 *  Created on: 18 avr. 2019
 *      Author: fab
 */

#include "Config_HW.h"
#include "stm32f10x.h"
#include "stm32f1xx_nucleo.h"

// fontions pour configuration

void configure_usart_rxtx(void)		// configuration USART via les 3 fcts Usart
{
    //const unsigned char welcome_str[] = " Welcome to Bluetooth!\r\n";

    /* Enable USART1 and GPIOA clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);

    /* NVIC Configuration */
    usart_NVIC_Configuration();

    /* Configure the GPIOs */
    usart_GPIO_Configuration();

    /* Configure the USART1 */
    usart_Configuration();

    /* Enable the USART1 Receive interrupt: this interrupt is generated when the
         USART1 receive data register is not empty */
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);		// défini dans stm32f10x_usart.h

    /* print welcome information */
    //UARTSend(welcome_str, sizeof(welcome_str));
}

void usart_GPIO_Configuration(void)		// ports utilisés par Usart 1 PA9 Tx, PA10 Tx
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Configure USART1 Tx (PA.09) as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Configure USART1 Rx (PA.10) as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void usart_Configuration(void)			// conf paramètres Usart
{
  USART_InitTypeDef USART_InitStructure;

/* USART1 configuration ------------------------------------------------------*/
  USART_InitStructure.USART_BaudRate = 115200;        // Baud Rate
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  USART_Init(USART1, &USART_InitStructure);

  /* Enable USART1 */
  USART_Cmd(USART1, ENABLE);
}


void usart_NVIC_Configuration(void)		// gestion prio interruption pour Usart
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Enable the USARTx Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5; //0 => 9 initialement
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

void acvHorlPortsABC(void)
{
	// validation de l'horloge des Ports A,B et C
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN;
}

void configure_gpio(GPIO_TypeDef *GPIO, int nChannel, int typage) 		// allume
{
	if (nChannel < 8)
	{
		// CRL
		GPIO->CRL &= ~(0xF << (4 * nChannel));		// Mise à 0 des 4 bits
		GPIO->CRL |= (typage << (4 * nChannel));	// 0 IAnalog, 1 OPushPull, 4 IFloating
	}
	else
	{
		//CRH
		nChannel -= 8;
		GPIO->CRH &= ~(0xF << (4 * nChannel));		// Mise à 0 des 4 bits
		GPIO->CRH |= (typage << (4 * nChannel));	// 0 IAnalog, 1 OPushPull, 4 IFloating
	}
}

void configure_adc_inX(void)
{

	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN; // validation horloge ADC1
    RCC->CFGR |= RCC_CFGR_ADCPRE_DIV6; 	// passage de l'horloge ADC1 à 12MHz
    ADC1->CR2|= ADC_CR2_ADON; 			// démarrage ADC1
    ADC1->SQR1&= ADC_SQR1_L; 			// fixe le nombre de conversion à 1 les 4 bits à 0
    ADC1->SQR3|= 8; 					// indique la voie à convertir 1er mesure

    ADC1->SMPR2 |= 5 << (8*3);			// Sample time 28.5 cycles pour avoir une bonne mesure
    ADC1->SMPR2 |= 5 << (4*3);			// Sample time	4:41.5 5:55.5
    ADC1->SMPR2 |= 5 << (1*3);			// Sample time
    ADC1->SMPR2 |= 5 ;					// Sample time

    ADC1->CR2 |= ADC_CR2_CAL; 			// début de la calibration
    while ((ADC1->CR2 & ADC_CR2_CAL)); 	// attente de la fin de la calibration
}

void sysTick_init (int loadVal)
{
	SysTick->CTRL = 0; 							// reset du registre
	SysTick->LOAD = loadVal;					// diviseur (719+1 72MHz => 100KHz, 1 tick toutes les 10 micros)
	SysTick->VAL = 0;							// car peut être à n'importe quelle valeur au réveil
	SysTick->CTRL |= SysTick_CTRL_CLKSOURCE; 	// direct pas divisé par 8
	SysTick->CTRL |= SysTick_CTRL_TICKINT;		// active l'interuption
	SysTick->CTRL |= SysTick_CTRL_ENABLE;		// active le compteur
}

void configure_itPC13(void) {
	// Configure EXTI13
	EXTI->IMR |= (0x01 << 13); // Validation de l'it externe des ports 13
	EXTI->FTSR |= (0x01 << 13); // Leve une it sur front descendant
	NVIC->ISER[1] |= NVIC_ISER_SETENA_8 ; // validation de l'IT 40 (EXTI10_15) dans le NVIC
	NVIC->IP[40] |= (7 << 4);	//priorité à 7
}

void configure_afio_exti_pc13(void) { 	//Configure le port PC13 comme source d'interruption externe
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	AFIO->EXTICR[3] &= ~(0x0F << 4);
	AFIO->EXTICR[3] |= (0x02 << 4);
}

void configure_timer(TIM_TypeDef *TIM, int psc, int arr) {
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN; // à completer pour les autres timers
	TIM->ARR = arr;
	TIM->PSC = psc;
}


void start_timer(TIM_TypeDef *TIM) {
	TIM->CR1 |= 0x01;
}

void configure_itTIM4(void) {
	// Interruption du timer 4
	TIM4->DIER |= TIM_DIER_UIE; // validation de l'it de TIM4
	NVIC->ISER[0] |= NVIC_ISER_SETENA_30 ; // validation de l'IT 30 (TIM4) dans le NVIC
	NVIC->IP[30] |= (8 << 4); 	//priorité à 8
}
