/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

/* reste à faire
ras
*/


/*
copie de essai_19 pour rajout bluetooth
Branchement du module bluetooth via USART1
(interruption N°37 prio = 9 non 5 pour ne pas perturber ouverture fermeture fichier)
Tx vers D2 (PA10)
Rx vers D8 (PA9)
Attention baud rate = 115200
*/

/*
gestion de la SD card, par le spi (connected to SPI1, PA 5-6-7 & PB 6)
.h requis ff ffconf diskio integer
.c requis ccsbc diskio ff
*/

/*
4 mesures analogiques
port B channel 0 => ADCIN 8 capteur delta Pression 5 bars (essence)
port A channel 4 => ADCIN 4 capteur delta Pression 1 bars (air)
port A channel 1 => ADCIN 1 Lambda Vri Tempé sonde
port A channel 0 => ADCIN 0 Lambda Vip Lambda
On change manuellement le channel avant chaque mesure.
(pas très catholique mais semble fonctionner correctement)
*/

/*
mesure du temps
Essai avec systick => semble OK
Attention j'ai l'impression qu'on tourne à 64Mhz ald 72
en théorie le load de systick doit être à 719,
mais dans la pratique 639 marche bcp mieux...
Est ce du au mode morpion (debug) qui peut ralentir à tester : NON
*/

/*
interruption appui bouton PC13 & PMH => semble OK
*/

/*
Fonction temporelle TIM4 à 1000ms
 */

/*
Gestion Led acqui en cours PC0
*/

/*
Gestion output mth on pour lambda meter PC1
(Activation chauffe lambda)
mise à la masse via transistor si tournant
*/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include "stm32f10x.h"
#include "stm32f1xx_nucleo.h"
#include "Config_HW.h"
#include "Com_HW.h"
#include "ff.h"			// pour gérer le system fat de la sd inclue integer.h (gestion entier) et ffconf.h (option FAT)
						// ff appele diskio.h qui gere le spi (SD card is connected to SPI1, PA 5-6-7 & PB6)
#include "stm32f10x_usart.h" // gestion bluetooth USART
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "misc.h"

/* Prototype des fonctions -----------------------------------------------------*/
void mesureAna(void);
void emissionBT(void);
void lanceAcqui(void);
void arreteAcqui(void);


/* Variables globales ----------------------------------------------------------*/
FRESULT fr;					// File function return code (enum) défini dans ff.h
FATFS fs;					// File system object structure (FATFS) défini dans ff.h
FIL fil;					// File object structure défini dans ff.h
char buff[32];				// Chaine de caractère à écrire dans le fichier
//int entry = 0;			// nb acquis (nb lignes)
int doLog = 1;				// mettre à 1 pour faire acqui
char logName[64];			// nom du fichier
volatile uint32_t counter = 0; // pour le temps
float tabVphy[5]; 			// 0 régime(delta T), 1.2 pression, 3.4 lambda
const int indChannelAna[5] = {13, 8, 4, 1, 0}; // 13 (non utilisé), PB0, PA4, PA1, PA0
int trigTmp = 1; 			// si 1 dernière mesure par temporel 100ms, si 0 par PMH
int UsartRxData;			// donnée reçue par bluetooth
int nbcall = 0;				// incrémenter toutes les secondes si le moteur est off si =100 arrêt acqui
int EmBT = 0;				// à 1 si besoin d'émission Bluetooth

int main(void)
{
	acvHorlPortsABC();							// activation horloges des ports ABC
	configure_gpio(GPIOC, 0, 1);				// PC0 Sortie pushpull Led rouge acquis en cours
	if (doLog) 									// Si acqui
	{
		lanceAcqui();
	}
	configure_usart_rxtx();
	configure_gpio(GPIOB, 0, 0);				// PB0 Entrée analogique, capteur delta Pression 5 bars (essence)
	configure_gpio(GPIOA, 4, 0);				// PA4 Entrée analogique, capteur delta Pression 1 bars (air)
	configure_gpio(GPIOA, 1, 0);				// PA1 Entrée analogique, Lambda Vri Tempé
	configure_gpio(GPIOA, 0, 0);				// PA0 Entrée analogique, Lambda Vip Lambda
	configure_gpio(GPIOC, 13, 4);				// PC13 Entrée régime (bobine) Input Floating
	configure_gpio(GPIOC, 1, 1);				// PC1 Sortie pushpull Activation chauffe lambda
	configure_adc_inX();
	sysTick_init (639);				// 1 pulse pour les 10 micros (théo 719 réel 639 car 64Mhz)
	configure_itPC13();
	configure_afio_exti_pc13();
	configure_timer(TIM4, 1279, 49999); // 100 ms (théo 719/9999 réel 639/9999) 1000ms(1279/49999)
	start_timer(TIM4);
	configure_itTIM4();
	start_timer(TIM4);

	while(1)
		{
		}

	return 0;

}

// interruptions

void SysTick_Handler(void) {
  counter++; // toutes les 10 micros
}

void EXTI15_10_IRQHandler(void){	// sur appui bouton user / PMH

	static int TropVite = 1;

	if (counter > 600) // N < 5000rpm on réalise les mesures
	{
		tabVphy[0] = counter/(100.00 * TropVite); // calcul N à faire en déporté
		counter = 0;
		trigTmp = 0;				// mesure par PMH
		mesureAna();
		if (EmBT && TropVite == 1)
		{
			emissionBT();			// réalisée entre 2 PMH
		}
		TropVite = 1;
	}
	else				// N > 5000rpm pas de mesures mais incrément de TropVite et EmBT si besoin
	{
		TropVite ++;
		if (EmBT)
		{
			emissionBT();			// réalisée entre 2 PMH
		}
	}

	EXTI->PR |= (0x01 << 13); // validation de l'IT
}

void TIM4_IRQHandler(void){			// toutes les 1000ms

	if (trigTmp == 1)				// si 1000ms ss PMH mesure à 1000ms
	{
		tabVphy[0] = 1000.00; 		// calcul N à faire en déporté
		mesureAna();
		nbcall ++;
		emissionBT();				// envoie bluetooth immédiat
		reset_gpio(GPIOC, 1);		// sortie PC1 à 0v : pas de chauffe sonde
	}
	else							// si PMH vu on resette les 100s et autorise la chauffe
	{
		nbcall = 0;
		EmBT = 1;					// envoie bluetooth après prochain PMH
		set_gpio(GPIOC, 1);			// sortie PC1 à 5v : chauffe sonde
	}

	if ((nbcall == 100) && doLog)		// 100s sans moteur tournant => arrêt acqui
	{
		arreteAcqui();
	}

	trigTmp = 1;				// mesure à 1000ms

	TIM4->SR &= ~TIM_SR_UIF ; // validation de l'IT dans le periph
}

void USART1_IRQHandler(void) // data bluetooth Rx dispo
{
    if ((USART1->SR & USART_FLAG_RXNE) != (u16)RESET)		// data Rx dispo
    {
    	UsartRxData = USART_ReceiveData(USART1);
        if(UsartRxData == '1' && !doLog){					// dmd acqui ON
        	lanceAcqui();
        }
        else if(UsartRxData == '0' && doLog){						// dmd acqui OFF
        	arreteAcqui();
        }
    }
}

// Autres foncions

void mesureAna(void)
{
	for (int i = 1; i < 5; i++)
	{
		tabVphy[i] = convert_single(indChannelAna[i]) * 5.5 / 0xFFF; // mesure de toutes les tensions
	}

	// passage tension => physique pour chacun à faire en dessous.
	//tabVphy[1] = tabVphy[1]*12.12 + 24.24;

	// écriture carte SD
	if (doLog && fr == FR_OK) 									// Si acqui et carte prête
	{
		// project properties > C/C++ Build > Setings > Tool Settings (TAB) > MCU GCC Linker > Miscellaneous > Linker flags), add “-u _printf_float”
		sprintf(buff, "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t\n", fminf(tabVphy[0], 999.00), tabVphy[1], tabVphy[2], tabVphy[3], tabVphy[4]); // définition de la ligne a ecrire
		f_puts(buff, &fil);		// ecriture ligne
	}
}

void emissionBT(void)
{
	unsigned char buffUsart[32];// Chaine de caractère à émettre en bluetooth
	sprintf(buffUsart, "%d\t%.0f\t%.1f\t%.1f\t%.1f\t%.1f", doLog, fminf(tabVphy[0], 99.00), tabVphy[1], tabVphy[2], tabVphy[3], tabVphy[4]);
	UARTSend(buffUsart,strlen(buffUsart));	// envoie bluetooth
	EmBT = 0;
}

void lanceAcqui(void)
{
	PauseIT(1);								// mise en pause des interuptions
	doLog = 1;
	nbcall = 0;								// reset compteur mth off
	get_log_name();							// choix nom dispo
	print_header();							// ouverture + ecriture 1eres lignes & laisse ouvert !
	set_gpio(GPIOC, 0);						// allumage led rouge acquis en cours
	PauseIT(0);								// reprise interuptions
}

void arreteAcqui(void)
{
	PauseIT(1);								// mise en pause des interuptions
	doLog = 0;
	f_close(&fil);							// ferme le fichier
	f_mount(NULL, "", 0); 					// demonte le disque
	reset_gpio(GPIOC, 0);					// eteint led rouge acquis en cours
	PauseIT(0);								// reprise interuptions
}
