/*
 * Com_HW.c
 *
 *  Created on: 18 avr. 2019
 *      Author: fab
 */

#include "Com_HW.h"
#include "stm32f10x.h"
#include "stm32f1xx_nucleo.h"
#include <stdio.h>


void PauseIT(int a)
{
	if (a)		// Mettre en pause les interuptions pendant gestion fichier
	{
		//NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC->ISER[1] &= ~NVIC_ISER_SETENA_8 ; // dévalidation de l'IT 40 (EXTI10_15) dans le NVIC PMH
		NVIC->ISER[0] &= ~NVIC_ISER_SETENA_30 ; // dévalidation de l'IT 30 (TIM4) dans le NVIC 1000ms
	}
	else 		// Reprendre les interuptions
	{
		//NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC->ISER[1] |= NVIC_ISER_SETENA_8 ; // validation de l'IT 40 (EXTI10_15) dans le NVIC PMH
		NVIC->ISER[0] |= NVIC_ISER_SETENA_30 ; // validation de l'IT 30 (TIM4) dans le NVIC 1000ms
	}
}



// pilotage led rouge acquis en cours

void set_gpio(GPIO_TypeDef *GPIO, int n) 		// allume
{
	GPIO->ODR |= (0x01 << n);
}

void reset_gpio(GPIO_TypeDef *GPIO, int n)		// eteint
{
	GPIO->ODR &= ~(0x01 << n);
}


// Lecture analogique

int convert_single(int nChannel) // Mesure analogique
{
	//counter = 0;
	ADC1->SQR3 &= ~ADC_SQR3_SQ1;		// les 5bits à 0 pour le choix du canal de mesure
	ADC1->SQR3|= nChannel;				// renseignement channel à mesurer
	ADC1->CR2 |= ADC_CR2_ADON; 			// lancement de la conversion
    while(!(ADC1->SR & ADC_SR_EOC) );	// attente de la fin de conversion
    ADC1->SR &= ~ADC_SR_EOC; 			// validation de la conversion
    return ADC1->DR & ~((0x0F) << 12); 	// retour de la conversion
}

// Carte SD

FRESULT open_append(FIL* fp, /* [OUT] File object to create */
const char* path /* [IN]  File name to be opened */
) {

	/* Opens an existing file. If not exist, creates a new file. */
	fr = f_open(fp, path, FA_WRITE | FA_OPEN_ALWAYS);
	if (fr == 0) {
		/* Seek to end of the file to append data */
		fr = f_lseek(fp, f_size(fp));
		if (fr != 0)
			f_close(fp);
	}
	return fr;
}

void get_log_name() {
	uint16_t numToAppend = 1;

	sprintf(logName, "log_.csv");
	f_mount(&fs, "", 0);  										// monte le disque
	fr = FR_OK;
	do {
		fr = f_open(&fil, logName, FA_OPEN_EXISTING);			// ouvre un nouveau fichier avec incrément du nom
		if (fr == FR_OK) {
			f_close(&fil);
			sprintf(logName, "log_%d.csv", numToAppend);
			numToAppend++;
		}
	} while (fr == FR_OK);
}

void print_header() {
	f_mount(&fs, "", 0);
	/* Open or create a log file and ready to append */
	fr = open_append(&fil, logName);
	if (fr == FR_OK) {
		/* Append a line */
		sprintf(buff, "Truc a ecrire au tout début \n");
		f_puts(buff, &fil);
		f_puts("Delta_T ms\tUDelta_P1 V\tUDelta_P2 V\tULamba_1 V\tULamba_2 V\n", &fil);		// Titre des colonnes
		//f_close(&fil);
	}
}

// envoie via bus Usart au module bluetooth
void UARTSend(const unsigned char *pucBuffer, unsigned long ulCount)
{
    //
    // Loop while there are more characters to send.
    //
    while(ulCount--)
    {
        USART_SendData(USART1, (uint16_t) *pucBuffer++);
        /* Loop until the end of transmission */
        while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
        {
        }
    }
}
