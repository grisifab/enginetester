/*
 * Config_HW.h
 *
 *  Created on: 18 avr. 2019
 *      Author: fab
 */

#include "stm32f10x.h"
#include "stm32f1xx_nucleo.h"

#ifndef CONFIG_HW_H_
#define CONFIG_HW_H_

void configure_usart_rxtx(void);		// fait appel aux 3 suivants
void usart_NVIC_Configuration(void);
void usart_GPIO_Configuration(void);
void usart_Configuration(void);

void acvHorlPortsABC(void);
void configure_gpio(GPIO_TypeDef *GPIO, int nChannel, int typage);
void configure_adc_inX(void);
void sysTick_init (int loadVal);
void configure_itPC13(void);
void configure_afio_exti_pc13(void);
void configure_timer(TIM_TypeDef *TIM, int psc, int arr);
void start_timer(TIM_TypeDef *TIM);
void configure_itTIM4(void);


#endif /* CONFIG_HW_H_ */
