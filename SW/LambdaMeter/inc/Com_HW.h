/*
 * Com_HW.h
 *
 *  Created on: 18 avr. 2019
 *      Author: fab
 */

#include "ff.h"
#include "stm32f10x.h"
#include "stm32f1xx_nucleo.h"

#ifndef COM_HW_H_
#define COM_HW_H_

// Variables globales définies dans le main
extern FRESULT fr;					// File function return code (enum) défini dans ff.h
extern FATFS fs;					// File system object structure (FATFS) défini dans ff.h
extern FIL fil;						// File object structure défini dans ff.h
extern char buff[32];				// Chaine de caractère à écrire dans le fichier
extern char logName[64];			// nom du fichier

//Mettre en pause les interuptions
void PauseIT(int a);

// pilotage Led rouge acquis en cours
void set_gpio(GPIO_TypeDef *GPIO, int n);
void reset_gpio(GPIO_TypeDef *GPIO, int n);

// Lecture analogique
int convert_single(int nChannel);

// Carte SD
FRESULT open_append(FIL* fp, const char* path);			// Ouvre ou crée un fichier
void get_log_name();									// Définit nom de fichier inesxistant
void print_header();									// Ouvre puis écrit 1er ligne fichier

// bus usart pour bluetooth
void UARTSend(const unsigned char *pucBuffer, unsigned long ulCount);


#endif /* COM_HW_H_ */
