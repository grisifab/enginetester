Détails des éléments fournis :
DSCN0002.jpg : détail du montage de la sonde sur la ligne d'échappement.
DSCN0003.jpg : photo de l'ensemble sur la voiture.
log_2.csv : acquisition brute(en tension) du roulage.(15 minutes => 60.000 mesures de chaque paramètres)
Trait_log_2.csv : acquisition traitée passage en valeur physique de tous les paramètres.
Mat_Trait_log_2.csv : Analyse du roulage, pour définir la richesse et la pression essance, dans toutes les conditions moteur.
sauv.bmp : Affichage des cartographies de richesse/pression_essence/nb_mesure pour chaque plage moteur.
gauss.xlsx : répartition des valeurs de richesse mesurées.
